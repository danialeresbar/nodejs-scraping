const puppeteer = require('puppeteer');

(async () => {
    const browser = await puppeteer.launch({ headless: false });

    const page = await browser.newPage();
    await page.setViewport({
        width: 1280,
        height: 720,
        deviceScaleFactor: 1
    })

    await page.goto('https://www.survio.com/survey/d/N1M8B4I8V6N7X8E8O\n');
    await page.waitForSelector('.start.button.inline')
    await page.screenshot({ path: 'screens/survey1.jpg' });

    // Accessing the survey
    await page.click('.start.button.inline');
    await page.waitForSelector('#survey > div > div.footer.v3')
    await page.waitForSelector('#survey .align ')
    await page.screenshot({ path: 'screens/survey2.jpg' });

    // Wait for questions
    await page.waitForSelector('#survey > div > div:nth-child(1) > div > div > div > div > div > div:nth-child(2)')

    // Answering the questions
    await page.click('#survey > div > div:nth-child(1) > div > div > div > div > div > div:nth-child(2) > div:nth-child(1) > div > div.answers.single-choice > ul > li:nth-child(1)');
    await page.click('#survey > div > div:nth-child(1) > div > div > div > div > div > div:nth-child(2) > div:nth-child(2) > div > div.answers.single-choice > ul > li:nth-child(3)');
    await page.click('#survey > div > div:nth-child(1) > div > div > div > div > div > div:nth-child(2) > div:nth-child(3) > div > div.answers.single-choice > ul > li:nth-child(2)');
    await page.click('#survey > div > div:nth-child(1) > div > div > div > div > div > div:nth-child(2) > div:nth-child(4) > div > div.answers.single-choice > ul > li:nth-child(3)');
    await page.click('#survey > div > div:nth-child(1) > div > div > div > div > div > div:nth-child(2) > div:nth-child(5) > div > div.answers.single-choice > ul > li:nth-child(1)');
    await page.click('#survey > div > div:nth-child(1) > div > div > div > div > div > div:nth-child(2) > div:nth-child(6) > div > div.answers.single-choice > ul > li:nth-child(1)');
    await page.type('#text57823449', 'Torneos deportivos');

    // Submit my answers
    await page.click('#survey > div > div:nth-child(1) > div > div > div > div > div > div.footerV3 > div > div > div.desktop-footer > div.navigation > div:nth-child(2)')

    // Screenshot of the final stage
    await page.waitForSelector('#thank-you > div > div.syspage-center > div.logo > a')
    await page.screenshot({ path: 'screens/survey3.jpg' });

    await browser.close();
})();
